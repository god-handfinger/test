package Test0813;

public class Customer {
    private String firstName;
    private String lastName;
    private Account account;
    public Customer(String F,String L){
        this.firstName=F;
        this.lastName=L;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}

