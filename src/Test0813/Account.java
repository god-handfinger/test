package Test0813;

public class Account {
    private int id;
    private double balance;
    private double annuallnterestRate;

    public Account(int id, double balance, double annuallnterestRate) {
        this.id = id;
        this.annuallnterestRate = annuallnterestRate;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public double getAnnuallnterestRate() {
        return annuallnterestRate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setAnnuallnterestRate(double annuallnterestRate) {
        this.annuallnterestRate = annuallnterestRate;
    }
    public void withdraw(double amount){
        if(balance<amount){
            System.out.println("余额不足！");
        }
        else {
            balance-=amount;
            System.out.println("成功取出： "+amount);
        }
    }
    public void deposit(double amount){
        balance+=amount;
        System.out.println("成功存入： "+amount);
    }
}
