package banking3;

public class Account {
    private double balance;//账户余额

    public Account(double init_balance) {
        balance = init_balance;
    }

    public double getBalance() {
        return balance;
    }

    public boolean deposit(double amt) {//存钱
        balance += amt;
        return true;
    }

    public boolean withdraw(double amt) {//取钱
        if (balance >= amt) {
            balance -= amt;
            return true;
        } else {
            System.out.println("余额不足！");
            return false;
        }
    }
}

