package Test.test;

public class GeometricObject {
    private String color;
    private  double wseight;
    public GeometricObject(String color, double wseight){
        this.color= this.color;
        this.wseight=wseight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWseight() {
        return wseight;
    }

    public void setWseight(double wseight) {
        this.wseight = wseight;
    }
    public double findArea(){
        return  0.0;
    }
}
class Circle extends GeometricObject{
    private double radius;
    public Circle(double radius,String color,double weight){
        super(color,weight);
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double findArea() {
        return Math.PI*radius*radius;
    }
}
class MyRectangle extends GeometricObject{
    private double width;
    private double height;

    public MyRectangle(double width,double height,String color,double weight) {
        super(color,weight);
        this.width=width;
        this.height=height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double findArea() {
        return this.height*this.width;
    }
}

class TestObject{
    public static void main(String[] args) {
        TestObject t=new TestObject();
        Circle c=new Circle(2.0,"red",10);
        Circle c1=new Circle(2.0,"Green",10);
        MyRectangle m =new MyRectangle(3,4,"bule",10);
        boolean b=t.equalsArea(c,m) ;
        System.out.println(b);
    }
    public boolean equalsArea(GeometricObject o1,GeometricObject o2){
//      if (o1.findArea() ==o2.findArea())
//          return  true;
//      else return false;
        return o1.findArea() ==o2.findArea();
    }
    public void displayGeometricObject(GeometricObject o){
        System.out.println(o.findArea());
    }
}
