package Test.test;

class Person{
    protected String name="zhangsan";
    protected  int age;
    public String getInfo(){
        return "Name:"+name+"\nage:"+age;
    }
}
class Student extends Person{
    protected String name="lisi";
    private String school="New Oriental";
    public String getSchool(){
        return school;
    }
    public String getInfo(){
        return super.getInfo()+"\nschool"+school;
    }
}

public class TestStudent {
    public static void main(String[] args) {
        Student student =new Student();
        System.out.println(student.getInfo());
    }
}
