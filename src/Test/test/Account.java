package Test.test;

public class Account {
    private int id;
    private double balance;
    private  double annualInterest;
    public Account(int id,double balance,double annualInterest){
        this.annualInterest=annualInterest;
        this.balance=balance;
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAnnualInterest() {
        return annualInterest;
    }

    public void setAnnualInterest(double annualInterest) {
        this.annualInterest = annualInterest;
    }
    public void withdraw(double amount){
        if(balance<=amount){
            System.out.println("余额不足");
        }
        else {
            balance-=amount;
            System.out.println("余额为"+balance);
        }
    }
    public void deposit(double amount){
        balance+=amount;
        System.out.println("存入成功，余额为"+balance);
    }

//    public static void main(String[] args) {
//        Account a = new Account(1122,20000,0.045);
//        a.withdraw(30000);
//        System.out.println(a.getBalance());
//        a.withdraw(2500);
//        a.deposit(3000);
//        System.out.println(a.getBalance()+" \n"+a.getAnnualInterest());
//    }
}
      class CheckAccount extends Account {
            private  double overfraft;

          public double getOverfraft() {
              return overfraft;
          }

          public void setOverfraft(double overfraft) {
              this.overfraft = overfraft;
          }

          public CheckAccount(int id, double init_balance, double
                                annualIterestRate, double overdraft){
                super(id,init_balance,annualIterestRate);
                this.overfraft=overdraft;
          }

          @Override
          public void withdraw(double amount) {
              if (getBalance()>=amount){
               setBalance(getBalance()-amount);
              }
              else if (overfraft>=amount-getBalance()){
                  this.overfraft-=(amount-getBalance());
                  setBalance(0);
              }
              else System.out.println("超过可透支额度！");
          }
      }
      class Test{
              public static void main(String[] args) {
        Account a = new Account(1122,20000,0.045);
        a.withdraw(30000);
        System.out.println(a.getBalance());
        a.withdraw(2500);
        a.deposit(3000);
        System.out.println(a.getBalance()+" \n"+a.getAnnualInterest());
        CheckAccount c=new CheckAccount(1122,20000,
                0.045,5000);
         c.withdraw(5000);
                  System.out.println("dangqian yue:"+c.getBalance());
                  System.out.println("tangzi:"+c.getOverfraft());
         c.withdraw(18000);
                  System.out.println("yu er:"+c.getBalance());
                  System.out.println("er du:"+c.getOverfraft());
         c.withdraw(3000);
                  System.out.println("yu er:"+c.getBalance());
                  System.out.println("er du:"+c.getOverfraft());

              }
      }