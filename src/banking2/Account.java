package banking2;

public class Account {
    private double balance;//账户余额

    public Account(double init_balance) {
        balance = init_balance;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amt) {//存钱
        balance += amt;
    }

    public void withdraw(double amt) {//取钱
        if (balance >= amt) {
            balance -= amt;
        } else {
            System.out.println("余额不足！");
        }
    }
}

