package bnaking5;

public class CheckingAccount extends Account{
    private double overdraftProtection;
    public CheckingAccount(double balance){
        super(balance);
    }

    public double getOverdraftProtection() {
        return overdraftProtection;
    }

    public void setOverdraftProtection(double overdraftProtection) {
        this.overdraftProtection = overdraftProtection;
    }

    public CheckingAccount(double balance, double overdraftProtection){
        super(balance);
        this.overdraftProtection=overdraftProtection;

    }
    public boolean withdraw(double amt) {//取钱
        if (balance >= amt) {
            balance -= amt;
            return true;
        }
        else if (overdraftProtection>=-(balance-amt)){
            overdraftProtection-=-(balance-amt);
            balance=0;
            return true;
        }
        else {
            System.out.println("余额不足！交易失败！");
            return false;
        }
    }

}
