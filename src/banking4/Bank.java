package banking4;

public class Bank {
    private Customer[] customers;//用于存放客户
    private int numberOfCustomers;

    public Bank() {
        customers = new Customer[5];
    }

    public void addCustomer(String F, String L) {//增加客户
        Customer cust = new Customer(F, L);
        customers[numberOfCustomers] = cust;
        numberOfCustomers++;
    }

    public int getNumberOfCustomers() {//获取客户个数
        return numberOfCustomers;
    }

    public Customer getCustomers(int index) {
        return customers[index];
    }
}
