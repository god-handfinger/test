package banking4;

public class Customer {
    private String firstName;
    private String lastName;
    private Account account;

    public Customer(String f, String L) {//设置客户姓名
        firstName = f;
        lastName = L;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {//设置客户账户
        this.account = account;
    }
}
